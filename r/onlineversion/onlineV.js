// urls
function addScript(src) {
  if (!document.getElementsByTagName || !document.createElement || !document.appendChild) {
    return false;
  } else {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    document.getElementsByTagName("head")[0].appendChild(script);
    return true;
  }
}

function onlinerun() {
  alert("Welcome to Titan Software.");
  var url = prompt("What is the URL of the widget you want to open?");
  if (url=="") {
    runagain("You specified no URL. To rerun press 'OK'");
  }
  else {
    if ( addScript(url) ) {
      JS_Run();
    } else {
      alert("Your widget url has failed!");
      runagain("Try again?");
    }
  }
  runagain("To run the program again, press 'OK'. Otherwise, press 'Cancel'.");
}

function runagain(text) {
  var rerun = confirm(text)
  if (rerun==true) {
    runagain();
  } else {
    console.log("");
  }
}
