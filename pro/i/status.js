function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function showStatus() {
  var Firebase = require("firebase");
  var ref = new Firebase("https://titansoftwarepro.firebaseio.com");
  var code = getUrlVars()["c"];

  ref.orderByChild(code+"/status").on(true, function(snapshot) {
    var name = snapshot.val().name;
    var address = snapshot.val().deliveryaddress;
    var statusVal = snapshot.val().status;
    var divCode = code;
  });

  document.getElementById("statusdiv").innerHTML = ("Reference Code: "+divCode+"<br>Name: "+name+"<br>Address: "+address+"<br>Status: "+statusVal);
}
