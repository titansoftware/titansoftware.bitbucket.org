function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";

    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function genURL() {
  var Firebase = require("firebase");
  var ref = new Firebase("https://titansoftwarepro.firebaseio.com");
  var products = ref.child("status");
  var name = document.getElementById("fullname").innerHTML
  var address = document.getElementById("address").innerHTML + document.getElementById("city").innerHTML + document.getElementById("country").innerHTML + document.getElementById("zipcode").innerHTML;
  var code = makeid();

  products.update({
    code+"/status": "Processing product"
    code+"/name": name
    code+"/deliveryaddress": address
    code+"/refcode": code
  });

  document.getElementById("urlCheck").innerHTML =
  ("Thank you for purchasing Titan Software Pro.<br>To view the status of your order go to this site:<br><input type='text' value='http://titansoftware.bitbucket.org/pro/i?c="+code+"/'></input>");
}
